$(window).on(VIEW_EVENT.READY, viewReady);

var indicadores = scorm.loadObject('indicadores') || {
	score: [{
		uid: "init",
		valor: 0
	}]
};

var indicadoresMax = {
	score: 10
};

bridge.addIndicador = function (indicador, valor, uid) {
	var newdrop = {
		uid,
		valor
	};

	var pool = indicadores[indicador];
	var countIfExist = pool.reduce((total, drop) =>
		(drop.uid == newdrop.uid ? total + 1 : total), 0);
	if (countIfExist == 0) {
		pool.push(newdrop);
	}

	indicadores[indicador] = pool;
	scorm.saveObject('indicadores', indicadores);

	setScoreQuiz(localStorage.getItem('_template-quiz'), bridge.getIndicadorTotal(indicador));

	this.emit('indicador_update', {
		data: bridge.getIndicadorTotal(indicador)
	});
};

function setScoreQuiz(template, score) {
	const selector = $("body").find(".feedback.feedback-positivo.hide").length;

	var data = JSON.parse(localStorage.getItem("cmi.suspend_data")).indicadores.score;
	var countData = JSON.parse(localStorage.getItem("cmi.suspend_data")).indicadores.score.length;

	if(template == '1' && data[countData -1].valor == 1) {
		localStorage.setItem('_score-quiz-1', parseInt(localStorage.getItem('_score-quiz-1')) + 1);
	}

	if(template == '2' && data[countData -1].valor == 1) {
		localStorage.setItem('_score-quiz-2', parseInt(localStorage.getItem('_score-quiz-2')) + 1);
	}
}

function applyScoreHTML(template) {
	if(template == 1) {
		// $(".indicador .counter .number").text(localStorage.getItem('_score-quiz-1') * 20);
		// $(".indicador .front").css("height", localStorage.getItem('_score-quiz-1') * 20);

		$(".indicador.indicador-coca .front").height(60);
	}else {
		// $(".indicador .counter .number").text(localStorage.getItem('_score-quiz-2') * 20);
		// $(".indicador .front").css("height", localStorage.getItem('_score-quiz-2') * 20);

		$(".indicador.indicador-coca .front").height(60);
	}
}

bridge.getIndicadorTotal = function (indicador) {
	var total = 0;
	var pool = indicadores[indicador];

	for (const drop of pool) {
		total += drop.valor;
	}

	applyScoreHTML(localStorage.getItem('_template-quiz'));

	return total;
};

bridge.getIndicadorPercentage = function (indicador) {
	var total = 0;
	var pool = indicadores[indicador];

	for (const drop of pool) {
		total += drop.valor;
	}

	if (localStorage.getItem('_template-quiz') == 1) {
		return parseInt(localStorage.getItem('_score-quiz-1')) / indicadoresMax[indicador] * 100;
		// return total / indicadoresMax[indicador] * 100;
	}

	if (localStorage.getItem('_template-quiz') == 2) {
		return parseInt(localStorage.getItem('_score-quiz-2')) / indicadoresMax[indicador] * 100;
	}

	// applyScoreHTML(localStorage.getItem('_template-quiz'));

	// return total / indicadoresMax[indicador] * 100;
};

function viewReady() {
	if (window.scorm.loadObject('iniciar') == true) {
		esconderCover();
	}
	bridge.next = function () {
		view.next();
	};

	$('#start').click(esconderCover);

	function esconderCover() {
		window.scorm.saveObject('iniciar', true);
		$('#cover').addClass('hide');
	}
}