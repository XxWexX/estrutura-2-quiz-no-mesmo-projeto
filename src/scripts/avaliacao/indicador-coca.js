events.on('ready', function () {
	bridge.on('indicador_update', function (e) {
		try {
			updateIndicadorCoca(bridge.getIndicadorPercentage('score'), true)
		} catch (e) {}
	});
});
events.on('enterView', function (e) {
	updateIndicadorCoca(bridge.getIndicadorPercentage('score'), false)
});

function updateIndicadorCoca(amount, animated) {
	var percentage = amount + "%";
	if (animated) {
		$('.indicador-coca .number').each(function () {
			$(this).numerator({
				toValue: amount
			});
		});
		$('.indicador-coca .front').first().animate({
			'height': percentage
		});
	} else {
		$('.indicador-coca .number').each(function () {
			$(this).html(amount);
		});
		$('.indicador-coca .front').first().css('height', percentage);
	}
}