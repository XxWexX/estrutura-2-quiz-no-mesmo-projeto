events.on('ready', function () {
	var percentage = bridge.getIndicadorPercentage('score');
	$('.score').html(percentage + "%");
	$('.ball').html('0%');
	setTimeout(function () {
		$('.mercurio').css('height', percentage + '%');
		var contagem = 0;
		var parts = 100;
		var interval = setInterval(function () {
			contagem += percentage / parts;
			$('.ball').html(Math.ceil(contagem) + '%');
			if (contagem >= percentage) {
				clearInterval(interval);
			}
		}, 1000 / parts);
	}, 1000);
});