//vendors
const path = require("path");
const gulp = require("gulp");
const del = require("del");
const open = require("gulp-open");
const less = require("gulp-less");
const babel = require("gulp-babel");
const concat = require("gulp-concat");
const connect = require("gulp-connect");
const sourcemaps = require("gulp-sourcemaps");
const scopackager = require('simple-scorm-packager');

//libs
const writer = require('./sys/utils/writer');
//configs
const SCORM_CONFIG = require('./scorm');
const BABEL_CONFIG = require('./sys/config/babel');
const SERVER_CONFIG = require('./sys/config/server');
const PAGES_CONFIG_FILE = path.resolve(__dirname, "./dist/app/config.js");
//paths
const SRC_PAGES_PATH = path.join(__dirname, './src', 'pages');
const DIST_PAGES_PATH = './dist/app/pages';
const FLAMBOYANT_CONFIG_PATH = path.resolve('./flamboyant.json');

//custom libs
const modulesManager = require('./sys/flamboyant/modules-manager');
const pagesBuilder = require('./sys/flamboyant/pages').init(SRC_PAGES_PATH, DIST_PAGES_PATH, BABEL_CONFIG);
const functions = require('./sys/utils/functions');

const cleanDist = () => del('./dist');


function buildIndexPug() {
	return pagesBuilder.buildOnePage('src/index.pug', 'dist');
}

function checkfolderList(...list) {
	let promises = [];
	for (const folder_path of list) {
		promises.push(functions.checkAndCreateFolder(path.resolve(folder_path)));
	}
	return Promise.all(promises);
}

function checkDist() {
	return checkfolderList('./dist');
}

function checkOthers() {
	return checkfolderList('./src/pages', './dist/app');
}
const checkfolders = gulp.series(checkDist, checkOthers);

function compileJs() {
	return gulp
		.src(['src/js/**/*.js'])
		.pipe(sourcemaps.init())
		.pipe(babel(BABEL_CONFIG))
		.pipe(concat("main.bundle.js"))
		.pipe(sourcemaps.write("."))
		.pipe(gulp.dest('dist/app/template'))
		.pipe(connect.reload());
}

function compileLess() {
	return gulp
		.src('src/less/*.less')
		.pipe(sourcemaps.init())
		.pipe(
			less({
				paths: [path.join(__dirname, './src/less', "includes")]
			})
		)
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('dist/app/template'))
		.pipe(connect.reload());
}

function watchFiles() {
	gulp.watch('./src/**/*.pug', buildIndexPug);
	gulp.watch('./src/**/*.pug', buildPages);
	gulp.watch('./src/pages/**/*.less', buildPages);
	gulp.watch('./src/pages/**/*.js', buildPages);
	gulp.watch('./src/less/**/*.less', compileLess);
	gulp.watch('./src/js/**/*.js', compileJs);
	gulp.watch('./src/scripts/**/*.js', compileScripts);
	gulp.watch('./src/assets/**/*', copyAssets);
	gulp.watch('./flamboyant_modules/**/dist/*', copyModules);

	gulp.src(__filename)
		.pipe(open({
			uri: "http://localhost:" + SERVER_CONFIG.port
		}));
	return connect.server(SERVER_CONFIG);
}

function copyModules() {
	const flamboyant = require('./flamboyant.json');
	let promises = [];
	for (const key in flamboyant.dependencies) {
		if (flamboyant.dependencies.hasOwnProperty(key)) {
			var from = path.resolve(__dirname, 'flamboyant_modules', key, 'dist', '*');
			var to = path.resolve(__dirname, 'dist', 'app', 'modules');
			promises.push(gulp.src(from).pipe(gulp.dest(to)));
		}
	}
	return Promise.all(promises);
}

function copyFiles(from_folder, to_folder) {
	var from = path.resolve(__dirname, 'src', from_folder, '**/*');
	var to = path.resolve(__dirname, 'dist', 'app', to_folder);
	return gulp.src(from).pipe(gulp.dest(to));
}

function copyVendor() {
	return copyFiles('vendor', 'vendor');
}

function compileScripts() {
	return gulp
		.src(['src/scripts/**/*'])
		.pipe(sourcemaps.init())
		.pipe(babel(BABEL_CONFIG))
		.pipe(sourcemaps.write("."))
		.pipe(gulp.dest('dist/app/scripts'))
		.pipe(connect.reload());
}

function compileScripts() {
	return copyFiles('scripts', 'scripts');
}

function copyAssets() {
	return copyFiles('assets', 'assets');
}

const buildPages = gulp.series(pagesBuilder.clean, pagesBuilder.createAll);
const defaulBuild = gulp.series(compileScripts, copyAssets, copyVendor, copyModules, compileJs, buildIndexPug, buildPages, compileLess);
const watchall = gulp.series(cleanDist, defaulBuild, configCreate, watchFiles);

/** utilitatios */

const configWriter = require('./sys/flamboyant/config-writer');

function configCreate() {
	return configWriter.create(PAGES_CONFIG_FILE, path.resolve(__dirname, './src', 'pages'));
}

function configUpdate() {
	return configWriter.update(PAGES_CONFIG_FILE, {
		debug: false
	});
}

function makeScormPack() {
	return new Promise((res, rej) => {
		scopackager(SCORM_CONFIG, function (msg) {
			res();
		});
	});
}

function doInstall() {
	return modulesManager.Dependencies.install(__dirname, FLAMBOYANT_CONFIG_PATH);
}

function doUpgrade() {
	return modulesManager.Dependencies.upgrade(__dirname, FLAMBOYANT_CONFIG_PATH);
}

function doUpdate() {
	return modulesManager.Dependencies.update(__dirname, FLAMBOYANT_CONFIG_PATH);
}

function installAutomationsCommands() {
	let config_package = require(path.resolve('./package.json'));
	let automations = require(FLAMBOYANT_CONFIG_PATH).automations;
	for (const automation in automations) {
		if (automations.hasOwnProperty(automation)) {
			let automation_command = "automation:" + automation;
			config_package.scripts[automation_command] = `gulp --gulpfile .\\sys\\automation\\${automation}`;
		}
	}
	return writer.writeStringToFile(path.resolve('./package.json'), JSON.stringify(config_package, null, 4));
}

function doAutomationInstall(cb) {
	return modulesManager.Automation.install(__dirname, FLAMBOYANT_CONFIG_PATH).then(installAutomationsCommands);
}

function doAutomationUpgrade() {
	return modulesManager.Automation.upgrade(__dirname, FLAMBOYANT_CONFIG_PATH);
}

function doAutomationUpdate() {
	return modulesManager.Automation.update(__dirname, FLAMBOYANT_CONFIG_PATH);
}

const doScorm = gulp.series(defaulBuild, configUpdate, makeScormPack);

/** Declarações de comandos para gulp */
gulp.task("config", configCreate);
gulp.task("build", defaulBuild);
gulp.task("default", watchall);
gulp.task("scorm", doScorm);

gulp.task('install', doInstall);
gulp.task('update', doUpdate);
gulp.task('upgrade', doUpgrade);

gulp.task('automation:install', doAutomationInstall);
gulp.task('automation:update', doAutomationUpdate);
gulp.task('automation:upgrade', doAutomationUpgrade);

gulp.task('prepare', gulp.series(cleanDist, checkfolders, doInstall, doAutomationInstall, configCreate));