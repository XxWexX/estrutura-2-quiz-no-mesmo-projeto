module.exports = {
	version: '1.2',
	organization: 'FEMSA Metodologias Ágeis',
	title: 'FEMSA Metodologias Ágeis',
	language: 'pt-BR',
	identifier: '00',
	masteryScore: null,
	startingPage: 'index.html',
	source: './dist',
	package: {
		version: "1.0.0",
		zip: true,
		outputFolder: './scorm'
	}
};