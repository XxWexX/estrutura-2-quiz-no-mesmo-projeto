const fs = require("fs");
const path = require("path");

const isDirectory = source => fs.lstatSync(source).isDirectory();

const getDirectories = source =>
	fs.readdirSync(source).map(name => path.join(source, name)).filter(isDirectory);

function getPageName(pagepath) {
	let exploded = pagepath.replace(/\\/g, '/').split('/');
	let pagename = exploded[exploded.length - 1];
	return pagename;
}

function getUserHome() {
	return process.env.HOME || process.env.HOMEPATH || process.env.USERPROFILE;
}

function checkAndCreateFolder(folder_path) {
	return new Promise((resolve, reject) => {
		if (!fs.existsSync(folder_path)) {
			fs.mkdirSync(folder_path);
			setTimeout(() => resolve(folder_path), 10);
		} else {
			resolve(folder_path);
		}
	});
}

function replaceAll(target, search, replacement) {
	return target.replace(new RegExp(search, 'g'), replacement);
}

module.exports = {
	isDirectory: isDirectory,
	getDirectories: getDirectories,
	getPageName: getPageName,
	checkAndCreateFolder: checkAndCreateFolder,
	replaceAll: replaceAll,
	getUserHome: getUserHome
};