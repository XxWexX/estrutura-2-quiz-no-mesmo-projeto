const fs = require("fs");
const beautifyjs = require('js-beautify').js;

function writeJsObjectToFile(filepath, contents) {
	return writeStringToFile(filepath, beautifyjs(contents, {
		indent_size: 4,
		space_in_empty_paren: false
	}));
}

function writeStringToFile(filepath, contents) {
	return new Promise(function (res, rej) {
		const stream = fs.createWriteStream(filepath);
		stream.once('open', function (fd) {
			stream.write(contents);
			stream.end();
			res();
		});
	});
}

module.exports = {
	jsObjectToFile: writeJsObjectToFile,
	writeStringToFile: writeStringToFile
};