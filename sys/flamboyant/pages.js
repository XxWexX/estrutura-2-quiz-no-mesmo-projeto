const functions = require('../utils/functions');
const del = require("del");
const fs = require("fs");
const path = require("path");
const gulp = require("gulp");
const pug = require("gulp-pug");
const less = require("gulp-less");
const babel = require("gulp-babel");
const sourcemaps = require("gulp-sourcemaps");
const concat = require("gulp-concat");

module.exports = {

	init: function (src_path, dist_path, BABEL_CONFIG) {
		function clean() {
			return del(['./dist/app/pages']);
		}

		function createAll() {
			let pages = functions.getDirectories(src_path);
			let promises = pages.map(buildPage);
			return Promise.all(promises);
		}

		function buildPage(pagepath) {
			let distpath = pageFixDistPath(pagepath);
			return Promise.all([
				buildPagePug(pagepath, distpath),
				buildPageStyle(pagepath, distpath),
				buildPageScript(pagepath, distpath)
			]);
		}

		function pageFixDistPath(pagepath) {
			return path.join(dist_path, functions.getPageName(pagepath));
		}

		function buildPagePug(pagepath, distpath) {
			return buildOnePage(path.join(pagepath, 'index.pug'), distpath);
		}

		function buildOnePage(src, dest) {
			return gulp
				.src(src)
				.pipe(pug({
					"pretty": true
				}))
				.pipe(gulp.dest(dest));
		}

		function buildPageScript(pagepath, distpath) {
			let srcpath = path.join(pagepath, 'script.js');
			if (!fs.existsSync(srcpath)) return;
			return gulp
				.src(['node_modules/@babel/polyfill/dist/polyfill.js', srcpath])
				.pipe(sourcemaps.init())
				.pipe(babel(BABEL_CONFIG))
				.pipe(concat("script.js"))
				.pipe(sourcemaps.write("."))
				.pipe(gulp.dest(distpath));
		}

		function buildPageStyle(pagepath, distpath) {
			let srcpath = path.join(pagepath, 'style.less');
			if (!fs.existsSync(srcpath)) return;
			return gulp
				.src(srcpath)
				.pipe(less())
				.pipe(gulp.dest(distpath));
		}
		return {
			clean: clean,
			createAll: createAll,
			buildPagePug: buildPagePug,
			buildOnePage: buildOnePage
		}
	}
}