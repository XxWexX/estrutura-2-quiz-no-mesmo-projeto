const moduleManager = require('./modules-version-manager');
const automationManager = require('./automation-version-manager');

module.exports = {
	Dependencies: moduleManager,
	Automation: automationManager
};