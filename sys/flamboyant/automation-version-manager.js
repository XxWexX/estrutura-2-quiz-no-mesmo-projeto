const git = require("nodegit");
const fs = require("fs");
const path = require("path");
const semver = require('semver');
const fn = require('../utils/functions');
const FLAMBOYANT_AUTOMATION_FOLDER_NAME = "automation";
const getModulesPath = (base_dir) => path.resolve(base_dir, 'sys', FLAMBOYANT_AUTOMATION_FOLDER_NAME);
const log = require('fancy-log');
const color = require('gulp-color');
const jswriter = require('../utils/writer');
let flamboyant;

function credentials(url, userName) {
	return git.Cred.sshKeyNew(
		userName,
		path.resolve(fn.getUserHome(), '.ssh/id_rsa.pub'),
		path.resolve(fn.getUserHome(), '.ssh/id_rsa'),
		"");
}

const fetchOpts = {
	callbacks: {
		credentials: credentials
	}
};

const cloneOptions = {
	checkoutBranch: "master",
	fetchOpts: fetchOpts
};

function repositoryURL(component_name) {
	return `git@gitlab.com:learning_solutions/flamboyant/automations/${component_name}.git`;
}

function checkIfModulesFolderExists(base_dir) {
	return fs.existsSync(getModulesPath(base_dir));
}

function checkAndCreateModulesFolder(base_dir) {
	if (!checkIfModulesFolderExists(base_dir)) {
		fs.mkdirSync(getModulesPath(base_dir));
	}
	return getModulesPath(base_dir);
}

function logComp(component) {
	return color('{' + component + '}', 'YELLOW');
}

function logVer(version) {
	return color(version, 'MAGENTA');
}

function pull(repository) {
	return repository.fetchAll({
		callbacks: {
			credentials: credentials,
			certificateCheck: function () {
				return 0;
			}
		}
	});
}

function checkoutVersion(repo, version, component) {
	return new Promise((res, rej) => {
		repo.getTagByName(version)
			.then(function (tag) {
				repo.getCommit(tag.targetId())
					.then(function (commit) {
						info(logComp(component), logVer(version), ' - ', commit);
						git.Checkout.tree(repo, commit, {
							checkoutStrategy: git.Checkout.STRATEGY.FORCE
						});
						res(version);
					}).catch(rej);
			}).catch(rej);
	});
}

function resolveDependency(FLAMBOYANT_MODULES_PATH, component, dependencies) {

	const localpath = path.resolve(FLAMBOYANT_MODULES_PATH, component);
	const version = getVersionName(dependencies, component);
	let promise = null;

	if (!fs.existsSync(localpath)) {
		promise = CloneRepo(component, localpath, version);
	} else {
		promise = UpdateRepo(component, localpath, version);
	}
	return promise;
}

function upgradeDependency(FLAMBOYANT_MODULES_PATH, component, dependencies) {
	const localpath = path.resolve(FLAMBOYANT_MODULES_PATH, component);
	const version = getVersionName(dependencies, component);
	return UpgradeRepo(component, localpath, version);
}

function updateDependency(FLAMBOYANT_MODULES_PATH, component, dependencies) {
	const localpath = path.resolve(FLAMBOYANT_MODULES_PATH, component);
	const version = getVersionName(dependencies, component);
	return UpdateRepo(component, localpath, version);
}


function getVersionName(dependencies, component) {
	return 'v' + semver.clean(dependencies[component]);
}

function UpdateRepo(component, localpath, version) {
	let _repository = null;
	let _version = version;
	return git.Repository.open(localpath)
		.then(repo => {
			_repository = repo;
			return repo;
		})
		.then(() => pull(_repository))
		.then(() => git.Tag.list(_repository))
		.then((list) => _version = getLastMinorVersion(list, version))
		.then(() => checkoutVersion(_repository, _version, component))
		.catch(error => info(logComp(component), error));
}

function getLastVersion(list) {
	if (list) {
		return list[list.length - 1];
	}
	return 'v0.0.0';
}

function getLastMinorVersion(list, _version) {
	let version = _version || 'v1.0.0';
	let major = semver.major(version);

	let new_version = 'v1.0.0';
	if (list) {
		for (let index = list.length - 1; index >= 0; index--) {
			const ver = list[index];
			let cmajor = semver.major(ver);
			if (cmajor == major) {
				new_version = ver;
				break;
			}
		}
	}
	return new_version;
}

function UpgradeRepo(component, localpath, version) {
	let _repository = null;
	let _version = version;
	return git.Repository.open(localpath)
		.then(repo => {
			_repository = repo;
			return repo;
		})
		.then(() => pull(_repository))
		.then(() => git.Tag.list(_repository))
		.then((list) => _version = getLastVersion(list))
		.then(() => checkoutVersion(_repository, _version, component))
		.catch(error => info(logComp(component), error));
}

function CloneRepo(component, localpath, version) {
	return git.Clone(repositoryURL(component), localpath, cloneOptions)
		.then(function (repo) {
			info("Instalando " + logComp(component));
			return checkoutVersion(repo, version, component);
		}).catch(error => info(logComp(component), error));

}

function info(...messages) {
	return new Promise((res, rej) => {
		let message = [color('\u273E Flamboyant', 'RED'), ">", color('Installer', 'CYAN'), ":"].concat(messages);
		log.info(message.join(" "));
		res();
	});
}

function install(base_dir, _flamboyant) {
	let flamboyant = require(_flamboyant);
	var FLAMBOYANT_MODULES_PATH = checkAndCreateModulesFolder(base_dir);
	let promises = [];
	let dependencies = flamboyant.automations;
	for (const component in dependencies) {
		if (dependencies.hasOwnProperty(component)) {
			promises.push(resolveDependency(FLAMBOYANT_MODULES_PATH, component, dependencies));
		}
	}
	return Promise.all(promises);
}

function updateConfig(_flamboyant_path, component, version) {
	if (version) {
		flamboyant.automations[component] = semver.clean(version);
	}
}

function upgrade(base_dir, _flamboyant) {
	flamboyant = require(_flamboyant);
	let promises = [];
	if (!checkIfModulesFolderExists(base_dir)) {
		return install(base_dir, _flamboyant);
	}
	let FLAMBOYANT_MODULES_PATH = getModulesPath(base_dir);
	let dependencies = flamboyant.automations;
	for (const component in dependencies) {
		if (dependencies.hasOwnProperty(component)) {
			promises.push(upgradeDependency(FLAMBOYANT_MODULES_PATH, component, dependencies)
				.then(version => updateConfig(_flamboyant, component, version)));
		}
	}
	return Promise.all(promises).then(() => jswriter.jsObjectToFile(_flamboyant, JSON.stringify(flamboyant)));
}

function update(base_dir, _flamboyant) {
	flamboyant = require(_flamboyant);
	let promises = [];

	if (!checkIfModulesFolderExists(base_dir)) {
		return install(base_dir, _flamboyant);
	}

	let FLAMBOYANT_AUTOMATIONS_PATH = getModulesPath(base_dir);
	let dependencies = flamboyant.automations;

	for (const component in dependencies) {
		if (dependencies.hasOwnProperty(component)) {
			promises.push(updateDependency(FLAMBOYANT_AUTOMATIONS_PATH, component, dependencies)
				.then(version => updateConfig(_flamboyant, component, version)));
		}
	}

	return Promise.all(promises).then(() => jswriter.jsObjectToFile(_flamboyant, JSON.stringify(flamboyant)));
}

module.exports = {
	install: install,
	upgrade: upgrade,
	update: update,
};