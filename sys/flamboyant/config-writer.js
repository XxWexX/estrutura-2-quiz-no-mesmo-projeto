const util = require('util');
const writer = require('../utils/writer');
const fn = require('../utils/functions');

function escreverPagina(page_name) {
	return {
		uid: page_name,
		src: `${page_name}/index.html`
	};
}

function escreverPaginas(pages_path) {
	return fn.getDirectories(pages_path).map(path_name => escreverPagina(fn.getPageName(path_name)));
}

function prepararParaSalvarConfig(config) {
	let conf = util.inspect(config, {
		showHidden: false,
		maxArrayLength: Infinity
	});
	return `var config=${conf}; try{ module.exports=config; } catch(e){ }`;
}

function escreverNovoConfig(salvarDados, debug, pages) {
	let config = {
		salvarDados: salvarDados,
		debug: debug,
		pages: pages
	};
	return prepararParaSalvarConfig(config);
}

function create(config_path, pages_path) {
	return writer.jsObjectToFile(config_path, escreverNovoConfig(true, true, escreverPaginas(pages_path)));
}

function update(config_path, updateObject) {
	var config = require(config_path);
	config = Object.assign(config, updateObject);
	return writer.jsObjectToFile(config_path, prepararParaSalvarConfig(config));
}

module.exports = {
	create: create,
	update: update
}